Source: dataquay
Section: libs
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Jaromír Mikeš <mira.mikes@seznam.cz>
Build-Depends:
 debhelper-compat (= 9),
 qtbase5-dev,
 librdf0-dev
Standards-Version: 3.9.8
Homepage: http://breakfastquay.com/dataquay/
Vcs-Git: https://salsa.debian.org/multimedia-team/dataquay.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/dataquay

Package: libdataquay0
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Pre-Depends:
 ${misc:Pre-Depends}
Description: Simple RDF for C++ and Qt applications
 Dataquay is a library that provides a friendly C++ interface
 to an RDF (Resource Description Framework) datastore
 using Qt4 classes and containers.
 Supported datastores are the popular and feature-complete Redland
 and the lightweight Sord.
 .
 It is principally aimed at Qt-based applications.

Package: libdataquay-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends:
 ${misc:Depends},
 libdataquay0 (= ${binary:Version}),
 librdf0-dev
Pre-Depends:
 ${misc:Pre-Depends}
Description: Simple RDF for C++ and Qt applications (development files)
 Dataquay is a library that provides a friendly C++ interface
 to an RDF (Resource Description Framework) datastore
 using Qt4 classes and containers.
 Supported datastores are the popular and feature-complete Redland
 and the lightweight Sord.
 .
 It is principally aimed at Qt-based applications.
 .
 This package contains the headers used to build applications
 that use libdataquay.
